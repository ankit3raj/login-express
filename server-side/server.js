const express = require("express");
const cors = require("cors");
const port = 2100;
const app = express();
const bodyParser = require("body-parser");
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const users = [{ email: "a@gmail.com", password: "123" }];

// app.use(function (req, res, next) {
//   console.log("Middleware called");
//   next();
// });
// app.post("/login", function (req, res) {
//   req.console.log("/user request called");
//   console.log(req.body);
//   req.res.send("");
// });
app.listen(port, () => console.log(`server is now listening on port ${port}`));

app.post("/login", (req, res) => {
  const isLoggedIn = users.find(
    (item) =>
      item.email === req.body.email && item.password === req.body.password
  );
  console.log(req.body);
  console.log(!!isLoggedIn);
  res.json({ isLoggedIn: !!isLoggedIn });
  // console.log(req.body);
});

app.post("/signup", (req, res) => {
  // users.push(req.body);
  const isSignedUp = users.find((item) => {
    item.email !== req.body.email;
  });
  console.log(!isSignedUp);
  if (!isSignedUp) {
    // console.log(users);
    // res.send(req.body);
    users.push(req.body);
    res.json({ isSignedUp: !isSignedUp });
  } else {
    res.json({});
  }
});

console.log(users);
